//
//  trackCell.swift
//  MattressGirl
//
//  Created by Adam Reis on 4/23/15.
//  Copyright (c) 2015 Adam Reis. All rights reserved.
//

import Foundation

@objc protocol TrackCellDelegate {
    optional func trackCellDidPressPlay(trackCell trackCell: TrackCell)
}

class TrackCell: UITableViewCell {
    @IBOutlet var trackLabel: UILabel!
    @IBOutlet weak var playPauseButton: UIButton!

    weak var delegate: TrackCellDelegate?
    
    var currentlyPlaying: Bool = false {
        didSet {
            if currentlyPlaying {
                playPauseButton.selected = true
            } else {
                playPauseButton.selected = false
            }
        }
    }

    @IBAction func didTapPlayButton(sender: AnyObject) {
        self.delegate?.trackCellDidPressPlay?(trackCell: self)
    }

}