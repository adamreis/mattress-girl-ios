//
//  SoundListTableViewController.swift
//  MattressGirl
//
//  Created by Adam Reis on 2/7/15.
//  Copyright (c) 2015 Adam Reis. All rights reserved.
//

import UIKit
import AVFoundation

class SoundListTableViewController: UITableViewController, UISearchResultsUpdating {

    var audioPlayer = AVAudioPlayer()
    var trackMap = Dictionary<String, NSURL>()
    var tracks = Array<String>()
    var filteredTracks = Array<String>()
    var resultSearchController = UISearchController()
    
    var currentAudioPlayer: AVAudioPlayer?
    var currentSelectedCell: TrackCell?
    var currentCellIndexPath: NSIndexPath?
    
    var downloadCount: Int = 0 {
        didSet {
            if downloadCount == 0 {
                self.refreshControl!.endRefreshing()
            }
            
            dispatch_async(dispatch_get_main_queue()) {
                self.tableView.reloadData()
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.addTarget(self, action: "refresh:", forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(refreshControl!)

        self.resultSearchController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            
            self.tableView.tableHeaderView = controller.searchBar
            
            return controller
        })()
    
        self.tableView.estimatedRowHeight = 100.0;
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        
        self.loadDownloadedSongs()
        
        self.refresh(self)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.tableView.reloadData()
    }
    
    func loadDownloadedSongs() {
        self.tracks = listFilesFromDocumentsFolder()
        for trackName in self.tracks {
            self.trackMap[trackName] = NSURL(fileURLWithPath: self.pathForFile(trackName))
        }
    }
    
    func listFilesFromDocumentsFolder() -> [String]
    {
        var theError = NSErrorPointer()
        let dirs = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.AllDomainsMask, true) as? [String]
        if dirs != nil {
            let dir = dirs![0]
            let fileList = try! NSFileManager.defaultManager().contentsOfDirectoryAtPath(dir)
            return fileList as! [String]
        }else{
            let fileList = [""]
            return fileList
        }
    }
    
    func refresh(sender:AnyObject) {
        let s3 = AWSS3.defaultS3()
        let listObjectRequest : AWSS3ListObjectsRequest = AWSS3ListObjectsRequest()
        listObjectRequest.bucket = "emmatron-recordings"

        let task1 = s3.listObjects(listObjectRequest)
        task1.continueWithBlock{ (task : AWSTask!) -> NSString in
            if (task.error != nil) {
                print("error: \(task.error)")
            } else {
                var newTrackList = [String]()
                let listObjectsOutput : AWSS3ListObjectsOutput = task.result as! AWSS3ListObjectsOutput
                let listObjectContents = listObjectsOutput.contents
                for file : AWSS3Object in listObjectContents  as! [AWSS3Object]!{
                    newTrackList.append(file.key!)
                }
                self.tracks = newTrackList
                self.syncTracks()
                
            }
            return ""
        }
        
    }
    
    func syncTracks() {
        // Delete unused tracks
        for track in listFilesFromDocumentsFolder() {
            if !self.tracks.contains(track) {
                try! NSFileManager.defaultManager().removeItemAtPath(self.pathForFile(track))
            }
        }
        
        // Save rest of tracks
        var fileManager = NSFileManager.defaultManager()
        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
        
        for trackName : String in self.tracks {
            let path : String = self.pathForFile(trackName)
            let pathURL = NSURL(fileURLWithPath: path)
            
            self.downloadCount++
            if fileManager.fileExistsAtPath(path) {
                self.trackMap[trackName] = pathURL
                self.downloadCount--
            } else {
                print("downloading \(path)")
                
                let readRequest : AWSS3TransferManagerDownloadRequest = AWSS3TransferManagerDownloadRequest()
                readRequest.bucket = "emmatron-recordings"
                readRequest.key = trackName
                readRequest.downloadingFileURL = pathURL
                
                let task = transferManager.download(readRequest)
                task.continueWithBlock { (task) -> AnyObject! in
                    if task.error == nil {
                        self.trackMap[trackName] = pathURL
                        self.downloadCount--
                    }
                    return nil
                }
            }
        }
    }
    
    func pathForFile(file: String) -> String {
        let folder = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as! NSString
        let path = folder.stringByAppendingPathComponent(file)
        return path
    }
    
    func stopSound() {
        try! AVAudioSession.sharedInstance().setActive(false)
    }
    
    func playSound(url: NSURL) -> AVAudioPlayer {
        try! AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
        try! AVAudioSession.sharedInstance().setActive(true)
        
        audioPlayer = try! AVAudioPlayer(contentsOfURL: url)
        audioPlayer.delegate = self
        audioPlayer.prepareToPlay()
        
        return audioPlayer
    }


    

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
        
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        
        if self.resultSearchController.active {
            return self.filteredTracks.count
        } else {
            return self.tracks.count
        }
    }


    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("trackCell", forIndexPath: indexPath) as! TrackCell
        
        
        self.configureCell(cell, indexPath: indexPath)

        return cell
    }
    
    func configureCell(cell: TrackCell, indexPath:NSIndexPath) {
        var track: NSString
        
        if self.resultSearchController.active {
            track = filteredTracks[indexPath.row] as! NSString
        } else {
            track = tracks[indexPath.row] as! NSString
        }
        
        if trackMap[track as String] == nil {
            cell.trackLabel.text = "Loading..."
        } else {
            cell.trackLabel.text = track.stringByDeletingPathExtension
        }
        
        cell.trackLabel.font = UIFont.preferredFontForTextStyle(UIFontTextStyleBody)

        cell.delegate = self
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let index = indexPath.row
        var importantTracks = self.tracks
        var audioPlayer: AVAudioPlayer? = nil
        
        if self.resultSearchController.active {
            importantTracks = self.filteredTracks
        }
        
        if importantTracks.count > index {
            let description = importantTracks[index]
            if trackMap[description] != nil {
                audioPlayer = playSound(trackMap[description]!)
            }
        }
        
        if let audioPlayer = audioPlayer,
            let cell = tableView.cellForRowAtIndexPath(indexPath) as? TrackCell {
                if cell == self.currentSelectedCell,
                    let currentAudioPlayer = self.currentAudioPlayer {
                        if cell.currentlyPlaying {
                            currentAudioPlayer.pause()
                            cell.currentlyPlaying = false
                        } else {
                            currentAudioPlayer.play()
                            cell.currentlyPlaying = true
                        }
                } else {
                    if let currentSelectedCell = self.currentSelectedCell,
                        currentAudioPlayer = self.currentAudioPlayer,
                        currentIndexPath = self.currentCellIndexPath {
                            currentSelectedCell.currentlyPlaying = false
                            currentAudioPlayer.stop()
                            self.tableView.deselectRowAtIndexPath(currentIndexPath, animated: true)
                    }
                    cell.currentlyPlaying = true
                    audioPlayer.play()
                    self.currentAudioPlayer = audioPlayer
                    self.currentSelectedCell = cell
                    self.currentCellIndexPath = indexPath
                }
            
        }
    }
    
    // MARK: - Searching

    func updateSearchResultsForSearchController(searchController: UISearchController)
    {
        if searchController.searchBar.text == "" {
            self.filteredTracks = self.tracks
        } else {
            let searchPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", searchController.searchBar.text!)
            let array = (self.tracks as NSArray).filteredArrayUsingPredicate(searchPredicate)
            self.filteredTracks = array as! [String]
        }

        self.tableView.reloadData()
    }
}

extension SoundListTableViewController: AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(player: AVAudioPlayer, successfully flag: Bool) {
        if player == self.currentAudioPlayer,
            let currentSelectedCell = self.currentSelectedCell,
            let currentCellIndexPath = self.currentCellIndexPath {
            currentSelectedCell.currentlyPlaying = false
            self.tableView.deselectRowAtIndexPath(currentCellIndexPath, animated: true)
        }
    }
}

extension SoundListTableViewController: TrackCellDelegate {
    func trackCellDidPressPlay(trackCell trackCell: TrackCell) {
        let indexPath = self.tableView.indexPathForCell(trackCell)
        self.tableView.selectRowAtIndexPath(indexPath, animated: false, scrollPosition: .None)
        self.tableView(self.tableView, didSelectRowAtIndexPath: indexPath!)
    }
}